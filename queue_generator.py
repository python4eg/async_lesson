import time

def task1(initial):
    a = initial
    while True:
        a *= 5
        time.sleep(2)
        yield a


def task2(initial):
    b = initial
    while True:
        b += 5
        time.sleep(2)
        yield b

queue = list()
queue.append(task1(2))
queue.append(task2(10))
queue.append(task2(50))
queue.append(task1(30))
while True:
    task_1_part = queue.pop(0)
    print(f'{task_1_part.__name__}: {next(task_1_part)}')
    queue.append(task_1_part)