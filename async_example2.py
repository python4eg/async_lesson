import asyncio

async def first():
    print('Executing first')
    result = 0
    for i in range(1, 11):
        print(f'Task 1: {i} step')
        result += i
        await asyncio.sleep(1)
    print(f'Task 1 Result: {result}')
    return result

async def second():
    print('Executing second')
    result = 1
    for i in range(1, 11):
        print(f'Task 2: {i} step')
        result *= i
        await asyncio.sleep(2)
    print(f'Task 2 Result: {result}')
    return result


event_loop = asyncio.get_event_loop()
try:
    event_loop.create_task(first())
    event_loop.create_task(second())
    event_loop.run_forever()
finally:
    event_loop.close()
