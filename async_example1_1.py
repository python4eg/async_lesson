import asyncio

async def first(name):
    print(f'Executing {name}')
    result = 0
    for i in range(1, 11):
        print(f'{name}: {i} step')
        result += i
        await asyncio.sleep(1)
    print(f'{name} Result: {result}')
    return result

async def second(name):
    print(f'Executing {name}')
    result = 1
    for i in range(1, 11):
        print(f'{name}: {i} step')
        result *= i
        await asyncio.sleep(1)
    print(f'{name} Result: {result}')
    return result

async def callback():
    result1 = await first('Task1')
    result2 = await second('Task2')
    return result1, result2

async def callback2():
    result1 = await first('Task3')
    result2 = await second('Task4')
    return result1, result2

event_loop = asyncio.get_event_loop()
try:
    event_loop.create_task(callback())
    event_loop.create_task(callback2())
    event_loop.run_forever()
finally:
    event_loop.close()
