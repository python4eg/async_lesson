import asyncio
import random

qu = asyncio.Queue(10)

async def producer():
    print('Executing producer')
    while True:
        if qu.full():
            print('Queue is full. Waiting!')
            await asyncio.sleep(4)
            continue
        print('Adding item')
        await qu.put(random.random() * 10)
        await asyncio.sleep(0.5)


async def first():
    print('Executing first')
    while True:
        if qu.empty():
            print('Queue is empty. Waiting!')
            await asyncio.sleep(1)
            continue
        value = await qu.get()
        print(f'{value}^2 = {value * value}')
        await asyncio.sleep(1.5)

async def second():
    print('Executing second')
    while True:
        if qu.empty():
            print('Queue is empty. Waiting!')
            await asyncio.sleep(1)
            continue
        value = await qu.get()
        await asyncio.sleep(1.5)
        print(f'2*{value} = {value + value}')

event_loop = asyncio.get_event_loop()
try:
    event_loop.create_task(producer())
    event_loop.create_task(first())
    event_loop.create_task(second())
    event_loop.run_forever()
finally:
    event_loop.close()
